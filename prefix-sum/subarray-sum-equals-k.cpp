class Solution {
public:
    int subarraySum(vector<int>& nums, int k) {
        unordered_map<int, int> prefix;
        int sum = 0;
        int count = 0;
        
        prefix[0] = 1;
        
        for (int num: nums) {
            sum += num;
            
            // get the complementary number and its frequency in the prefix map
            int com = sum - k;
            int freqCom = prefix.count(com) == 1 ? prefix[com] : 0;

            // Count how many subarrays there are that equal k
            count += freqCom;
            
            // Increase frequency of own prefix sum by one in the prefix map
            prefix[sum] = prefix.count(sum) == 1 ? prefix[sum] + 1 : 1;
        }
        
        return count;
    }
};
