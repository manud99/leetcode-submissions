class UnionFind {
private:
    vector<int> parents;
    vector<int> rank;
public:
    UnionFind(int n) {
        // initialize array with unique values for each key
        parents = vector<int>(n);
        iota(parents.begin(), parents.end(), 0);
        // the size of each tree is 1
        rank = vector<int>(n, 1);
    }
    
    int find(int node) {
        // find the root of the tree
        while (node != parents[node]) {
            node = parents[node];
        }
        return node;
    }
    
    void combine(int u, int v) {
        int parU = find(u);
        int parV = find(v);
        
        // append smaller tree to bigger tree,
        // so that the tree's height (rank) remains as small as possible
        if (rank[parU] > rank[parV]) {
            parents[parV] = parU;
        
        } else if (rank[parU] < rank[parV]) {
            parents[parU] = parV;
        
        } else {
            // both tree have the same size
            parents[parV] = parU;
            rank[parU]++;
        }
    }
};

class Solution {
public:
    int minCostConnectPoints(vector<vector<int>>& points) {
        int n = points.size();
        int result = 0;
        int counter = 0;
        
        // Kruskal's algorithm with UnionFind data structure
        UnionFind unionFind(n);
        
        // sort edges by their weight
        // size by Gaussian sum formula: (n-1) + (n-2) + ... + 2 + 1 + 0 = (n-1) * n / 2
        vector<vector<int>> edges;
        
        // custom edge comparator
        struct {
            bool operator()(vector<int> &a, vector<int> &b) const { return a[2] > b[2]; }
        } comp;
        
        // create edges array
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                edges.push_back(
                    vector<int>{i, j, abs(points[i][0] - points[j][0]) + abs(points[i][1] - points[j][1])}
                );
                push_heap(edges.begin(), edges.end(), comp);
            }
        }
        
        // print edges array
        // for (vector<int> edge : edges) {
        //     cout << edge[0] << " " << edge[1] << " " << edge[2] << "\n";
        // }
        
        // select the (n-1) smallest edges that form a minimum spanning tree
        while (counter < n - 1) {
            // get the smallest edge and restore heap condition
            pop_heap(edges.begin(), edges.end(), comp);
            vector<int> edge = edges.back();
            edges.pop_back();
            
            if (unionFind.find(edge[0]) == unionFind.find(edge[1])) continue;
            
            // mark that both vertices are connected through our spanning tree
            unionFind.combine(edge[0], edge[1]);
            result += edge[2];
            ++counter;
        }
        
        return result;
    }
};
