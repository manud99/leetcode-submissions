class Solution {
public:
    double maxProbability(int n, vector<vector<int>>& edges, vector<double>& succProb, int start, int end) {
        vector<vector<pair<int, double>>> adjList(n);
        vector<double> probs(n, 0.0);
        priority_queue<pair<double, int>> pq;
        
        for (int i = 0; i < edges.size(); i++) {
            adjList[edges[i][0]].push_back(make_pair(edges[i][1], succProb[i]));
            adjList[edges[i][1]].push_back(make_pair(edges[i][0], succProb[i]));
        }
        
        pq.push(make_pair(1.0, start));
        probs[start] = 1.0;
        
        while (!pq.empty()) {
            pair<double, int> u = pq.top();
            pq.pop();
            
            if (u.second == end) return probs[end];
            
            for (pair<int, double> v : adjList[u.second]) {
                if (probs[u.second] * v.second > probs[v.first]) {
                    probs[v.first] = probs[u.second] * v.second;
                    pq.push(make_pair(probs[v.first], v.first));
                }
            }
        }
                
        return 0.0;
    }
};
