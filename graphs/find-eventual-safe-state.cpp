class Solution {
public:
    bool dfs(int node, vector<vector<int>>& graph,
             vector<bool>& marked, vector<bool>& tempMarked) {
        tempMarked[node] = true;
        
        for (int child : graph[node]) {
            if (marked[child]) continue;
            if (tempMarked[child]) return false;
            
            if (!dfs(child, graph, marked, tempMarked)) return false;
        }
        
        marked[node] = true;
        return true;
    }
    
    vector<int> eventualSafeNodes(vector<vector<int>>& graph) {
        vector<int> res;
        vector<bool> marked(graph.size(), false);
        vector<bool> tempMarked(graph.size(), false);
        
        for (int i = 0; i < graph.size(); ++i) {
            if (dfs(i, graph, marked, tempMarked)) res.push_back(i);
        }
        
        return res;
    }
};
