class Solution {
public:
    int numIslands(vector<vector<char>>& grid) {
        int m = grid.size();
        int n = grid[0].size();
        vector<vector<int>> components(m, vector<int>(n, 0));
        stack<pair<int, int>> s;
        int cur = 1;
        
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                // while there are unvisited islands ...
                if (components[i][j] != 0 || grid[i][j] == '0') continue;
                
                // DFS
                s.push(pair(i, j));
                components[i][j] = cur++;

                while (!s.empty()) {
                    pair<int, int> u = s.top();
                    s.pop();

                    // go down
                    if (u.first < m - 1 && grid[u.first + 1][u.second] == '1' 
                            && components[u.first + 1][u.second] == 0) {
                        
                        s.push(pair(u.first + 1, u.second));
                        components[u.first + 1][u.second] = components[u.first][u.second];
                    }
                    
                    // go up
                    if (u.first > 0 && grid[u.first - 1][u.second] == '1' 
                            && components[u.first - 1][u.second] == 0) {
                        
                        s.push(pair(u.first - 1, u.second));
                        components[u.first - 1][u.second] = components[u.first][u.second];
                    }
                    
                    // go right
                    if (u.second < n - 1 && grid[u.first][u.second + 1] == '1' 
                            && components[u.first][u.second + 1] == 0) {
                        
                        s.push(pair(u.first, u.second + 1));
                        components[u.first][u.second + 1] = components[u.first][u.second];
                    }

                    // go left
                    if (u.second > 0 && grid[u.first][u.second - 1] == '1' 
                            && components[u.first][u.second - 1] == 0) {
                        
                        s.push(pair(u.first, u.second - 1));
                        components[u.first][u.second - 1] = components[u.first][u.second];
                    }
                }
            }
        }
        
        return cur - 1;
    }
};
