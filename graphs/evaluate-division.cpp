struct Node {
    string key;
    vector<Node*> edges;
    vector<double> weights;
    
    Node(string key) {
        this->key = key;
    }
};

class Solution {
public:
    vector<double> calcEquation(vector<vector<string>>& equations, vector<double>& values, vector<vector<string>>& queries) {
        int n = equations.size();
        unordered_map<string, Node*> nodes;
        
        for (int i = 0; i < n; ++i) {
            string u = equations[i][0];
            string v = equations[i][1];
            Node* nodeU;
            Node* nodeV;
            
            if (nodes.count(u) == 0) {
                nodeU = new Node(u);
                nodes.insert({u, nodeU});
            } else {
                nodeU = nodes[u];
            }
            
            if (nodes.count(v) == 0) {
                nodeV = new Node(v);
                nodes.insert({v, nodeV});
            } else {
                nodeV = nodes[v];
            }
            
            nodeU->edges.push_back(nodeV);
            nodeU->weights.push_back(values[i]);
            
            nodeV->edges.push_back(nodeU);
            nodeV->weights.push_back(1/values[i]);
            
        }
        
        vector<double> result;
        for (int i = 0; i < queries.size(); ++i) {
            string from = queries[i][0];
            string to = queries[i][1];
            
            if (nodes.count(from) == 0 || nodes.count(to) == 0) {
                result.push_back(-1.0);
                continue;
            }
            
            stack<Node*> stack;
            unordered_map<string, double> dist;
            
            stack.push(nodes[from]);
            dist[from] = 1.0;
            
            while(!stack.empty()) {
                Node* u = stack.top();
                stack.pop();
                
                for (int j = 0; j < u->edges.size(); ++j) {
                    Node* v = u->edges[j];
                    
                    if (dist.count(v->key) != 0) continue;
                    
                    stack.push(v);
                    dist[v->key] = dist[u->key] * u->weights[j];
                }
            }
            
            if (dist.count(to) == 0) {
                result.push_back(-1.0);
            } else {
                result.push_back(dist[to]);
            }
        }
        
        return result;
    }
};
