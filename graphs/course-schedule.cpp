class Solution {
private:
    bool dfs(int node, vector<vector<int>>& adjList,
             vector<bool>& marked, vector<bool>& tempMarked) {
        
        for (int out : adjList[node]) {
            if (marked[out]) continue;
            if (tempMarked[out]) return false;
            
            tempMarked[node] = true;
            if (!dfs(out, adjList, marked, tempMarked)) return false;
        }
        
        marked[node] = true;
        return true;
    }

public:
    bool canFinish(int numCourses, vector<vector<int>>& prerequisites) {
        vector<vector<int>> adjListIn(numCourses, vector<int>());
        vector<bool> marked(numCourses, false);
        vector<bool> tempMarked(numCourses, false);
        
        for (vector<int> prereq : prerequisites) {
            int from = prereq[0];
            int to = prereq[1];
            
            adjListIn[to].push_back(from);
        }
        
        for (int i = 0; i < numCourses; ++i) {
            if (marked[i]) continue;
            
            bool res = dfs(i, adjListIn, marked, tempMarked);
            
            if (!res) return false;
        }
        
        return true;
    }
};
