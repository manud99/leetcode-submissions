class Solution {
public:
    int rob(vector<int>& nums) {
        if (nums.size() == 1) return nums[0];
        if (nums.size() == 2) return max(nums[0], nums[1]);
        
        int f1 = nums[0];
        int s1 = max(nums[0], nums[1]);
        int n1 = 0;
        int f2 = nums[1];
        int s2 = max(nums[1], nums[2]);
        int n2 = 0;
        
        int n = nums.size() - 1;
        for (int i = 2; i <= n; ++i) {
            if (i < n) {
                n1 = max(f1 + nums[i], s1);
                f1 = s1;
                s1 = n1;
            }

            if (i > 2) {
                n2 = max(f2 + nums[i], s2);
                f2 = s2;
                s2 = n2;
            }
        }
        
        return max(max(f1, s1), max(f2, s2));
    }
};
