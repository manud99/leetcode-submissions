class Solution {
public:
    string longestPalindrome(string s) {
        int n = s.length();
        string res = s.substr(0, 1);
        int resLength = 1;
        
        // DP table
        vector<vector<int>> lengthMap(s.length(), vector<int>(s.length()));
        
        // Loop rowise from bottom up
        for (int i = n - 1; i >= 0; --i) {
            for (int j = i; j < n; ++j) {
                if (i == j) {
                    // Single letters are always palindroms
                    lengthMap[i][j] = 1;
                
                } else if (s.at(i) == s.at(j) && lengthMap[i + 1][j - 1] == j - i - 1) {
                    // Increase by two when diagonal element is a palindrom
                    lengthMap[i][j] = lengthMap[i + 1][j - 1] + 2;
                    
                    // Temporary store the longest result
                    if (lengthMap[i][j] > resLength) {
                        res = s.substr(i, j - i + 1);
                        resLength = lengthMap[i][j];
                    }
                    
                } else {
                    lengthMap[i][j] = max(lengthMap[i][j - 1], lengthMap[i + 1][j]);
                }
            }
        }
        
        return res;
    }
};
