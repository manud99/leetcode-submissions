/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
private:
    int* dfsRob(TreeNode* node) {
        // If the node not does not exist return 0
        if (!node) return new int[] {0, 0};
    
        // DFS search in the node's children
        int* left = dfsRob(node->left);
        int* right = dfsRob(node->right);
        
        // Calculate the solution with or without the current node
        int withNode = left[1] + right[1] + node->val;
        int withoutNode = left[0] + right[0];
        
        // Return two types of information:
        // - max possible value up to this node
        // - the value without the current node
        return new int[] {max(withNode, withoutNode), withoutNode};
    }
public:
    int rob(TreeNode* root) {
        int* res = dfsRob(root);
        
        return max(res[0], res[1]);
    }
};
