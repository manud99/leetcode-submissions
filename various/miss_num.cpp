#include <iostream>
#include <bits/stdc++.h>

using namespace std;

#define ll long long

bool test(int sum, int diff, int prod, int quot) {
    // in a possible solution (sum+diff) must be not 0 and divisible by 2
    ll twoA = sum + diff;
    if (twoA == 0 || twoA % 2 != 0) return false;
    
    /*
     * calcualte possible solution a and b according to formula:
     *
     * (a + b) + (a - b) = 2 * a
     * 
     *     2 * a * b       2 * a * b
     * ----------------- = --------- = b
     * (a + b) + (a - b)     2 * a
     */
    int a = twoA / 2;
    int b = prod * 2 / twoA;

    // b cannot be 0, otherwise division by zero
    if (b==0) return false;

    // check whether the possible solution is in our interval [1, 1000]
    // and whether they sum up to the four inputs
    return a >= 1 && a <= 10000 && b >= 1 && b <= 10000
            && a+b == sum && a-b == diff && a*b == prod && a/b == quot;
}

pair<int, int> algo(vector<int> &v) {
    // Go through all permutations
    sort(v.begin(), v.end());
    do {
        if (test(v[0], v[1], v[2], v[3])) {
            // calculate again a and b and return it
            ll twoA = v[0] + v[1];
            int a = twoA / 2;
            int b = v[2] * 2 / twoA;
            
            return {a, b};
        }
    } while (next_permutation(v.begin(), v.end()));

    return {-1, -1};
}

int main() {
    int t, a, b, c, d;

    cin >> t;

    // for each test case
    while (t-- > 0) {
        // read inputs
        cin >> a >> b >> c >> d;
        
        // find result
        vector<int> v = {a, b, c, d};
        auto [x, y] = algo(v);

        // print result
        cout << x << " " << y << "\n";
    }

    return 0;
}
